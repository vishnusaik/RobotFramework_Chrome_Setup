*** Settings ***
Documentation     ${screenshotDir} \ \ \ ./
...               ${sleepTime} \ \ \ \ \ 15s
...               ${longSleepTime} \ \ \ 15s
...               ${shortSleepTime} \ \ \ 6s
...               ${adminUserLogin} \ \ \ shopadm
...               ${adminUserPassword} \ \ \ shop
...               ${simpleUserLogin} \ \ \ shopusr1
...               ${simpleUserPassword} \ \ \ usr1
...               ${domain} \ \ \ "Période commerciale courante"



*** Variables ***
${sleepTime}      20s
${longSleepTime}    25s
${shortSleepTime}    15s


${browser}    chrome
${AppUrl}   https://the-internet.herokuapp.com/login
${UserName}     tomsmith
${UserPassword}     SuperSecretPassword!
