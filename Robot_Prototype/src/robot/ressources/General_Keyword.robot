*** Settings ***
Documentation     This resource file contains common and global variables
Library           OperatingSystem
Library           String    
Library           Screenshot
Library           SeleniumLibrary
Library           Collections

*** Keywords ***
AD: Login To App
    [Documentation]    Authentication method for logging the application with a specific user information.
    [Arguments]    ${UserName}    ${UserPassword}
#    Log    Test step: Se connecter au url :  ${AppUrl}  .    console=yes
#    Open Browser    ${AppUrl}    ${browser}
    Open Browser    ${AppUrl}    headlessfirefox
    Maximize Browser Window    
    Log    Test step: etting the user Name ${UserName}    console=yes
    Wait Until Page Contains Element    //*[@id="username"]
    Input Text    name=username    ${UserName}
    Input Password    name=password    ${UserPassword}
    Click Element   xpath=//*[@id="login"]/button/i





